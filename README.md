# Currency Converter

Currency Converter is a web application for easy and fast currency converter with live rates

## Description

Currency Converter is a web application, made for the purpose of performing tasks for college. It's made with HTML, CSS, and JavaScript. The simple UI allows the use of a fast and reliable API for currency conversion that refreshes exchange rates several times per second.
