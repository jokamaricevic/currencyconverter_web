
let countries = {"CAD" : "Canadian Dollar", 
"HKD" : "Hong Kong Dollar", 
"ISK" : "Icelandic Krona", 
"PHP" : "Philippene Peso", 
"DKK" : "Danish Krone", 
"HUF" : "Hungarian Forint", 
"CZK" : "Czech Koruna", 
"AUD" : "Australian Dollar", 
"RON" : "Romanian Leu",
"SEK" : "Swedish Krona",  
"IDR" : "Indonesian Rupiah", 
"INR" : "Indian Rupee", 
"BRL" : "Brazilian Real", 
"RUB" : "Russian Ruble", 
"HRK" : "Croatian Kuna", 
"JPY" : "Japanese Yen", 
"THB" : "Thai Baht", 
"CHF" : "Swiss Franc", 
"SGD" : "Singapore Dollar", 
"PLN" : "Polish Zloty", 
"BGN" : "Bulgarian Lev", 
"TRY" : "Turkish Lira", 
"CNY" : "Chinese Yuan",
"NOK" : "Norwegian Krone",
"NZD" : "New Zeland Dollar",
"ZAR" : "South Africa Rand",
"USD" : "US Dollar",
"MXN" : "Mexican Peso",
"ILS" : "Israel Shekel",
"GBP" : "British Pound",
"KRW" : "South Korean Won",
"MYR" : "Malaysian Ringgit"
}

function createDropDown()  {
    let html = ""
    Object.keys(countries).forEach(element => {
        html += "<option value='" + element + "'>" + element + " - " + countries[element] + "</option>\n"
    });

    const elements = document.getElementsByClassName("ddMenu");
    Array.prototype.slice.call( elements ).forEach(element => {
        element.innerHTML = html;
    });
}

createDropDown();let HttpClient = function() {
    this.get = function(aUrl, aCallback) {
        let anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function() { 
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
            aCallback(anHttpRequest.responseText);
        }
        anHttpRequest.open( "GET", aUrl, true ); 
        anHttpRequest.send(); 
    }
}

function getData(){
    let ddFrom =  document.getElementById("ddFrom");
    let from =  ddFrom.options[ddFrom.selectedIndex].value;
    let ddTo =  document.getElementById("ddTo");
    let to =  ddTo.options[ddTo.selectedIndex].value;
    
    let iAmount =  document.getElementById("iAmount");
    let amount =  iAmount.value;
    let theurl='https://api.exchangeratesapi.io/latest?base=' + from;
    let client = new HttpClient();
    let msg = document.getElementById("dErrorMsg");
    if(amount == 0){
        msg.innerHTML = "PLEASE ENTER AMOUNT";
    }else{
        client.get(theurl, function(response) { 
            let response1 = JSON.parse(response);
            setResults(Math.round(amount * 100)/100, from, Math.round(response1["rates"][to] * amount * 100)/100 , to,Math.round(response1["rates"][to] * 100)/100 );
        });
    }
}

function setResults(fromAmount, fromCurrency, toAmount, toCurrency, rate) {
    const postResultNote = "All figures are live mid-market rates, which are not available to consumers and are for informational purposes only."
    let sWordResultConversion = document.getElementById("sWordResultConversion");
    let sConvertResultFromAmount = document.getElementById("sConvertResultFromAmount");
    let sConvertResultFromCurrency = document.getElementById("sConvertResultFromCurrency");
    let sConvertResultToAmount = document.getElementById("sConvertResultToAmount");
    let sConvertResultToCurrency = document.getElementById("sConvertResultToCurrency");
    let pSideResultToFrom = document.getElementById("pSideResultToFrom");
    let pSideResultFromTo = document.getElementById("pSideResultFromTo");
    let pPostResult = document.getElementById("pPostResult");
    
    
    sWordResultConversion.innerHTML = countries[fromCurrency]  + " to " + countries[toCurrency] + " Conversion";
    sConvertResultFromAmount.innerHTML = fromAmount;
    sConvertResultFromCurrency.innerHTML = fromCurrency + " =";
    sConvertResultToAmount.innerHTML = toAmount;
    sConvertResultToCurrency.innerHTML = toCurrency;
    pSideResultToFrom.innerHTML = "1 " + toCurrency + " = " + Math.round(1/rate * 100)/100  + " " + fromCurrency;
    pSideResultFromTo.innerHTML = "1 " + fromCurrency + " = " + rate + " " + toCurrency;
    pPostResult.innerHTML = postResultNote;

    

}

var input = document.getElementById("iAmount");
input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
    event.preventDefault();
    getData();
  }
});

function setCompareResults(){
    let leftResults = "";
    let rightResults = "";
    let dLeftResults = document.getElementById("resultsLeft");
    let dRightResults = document.getElementById("resultsRight");
    let side = true;
    let client = new HttpClient();
    let dShow = document.getElementById("ddShow");
    let from = dShow.options[dShow.selectedIndex].value;
    let theurl='https://api.exchangeratesapi.io/latest?base=' + from;
    client.get(theurl, function(response) { 
        let response1 = JSON.parse(response);
        // setResults(Math.round(amount * 100)/100, from, Math.round(response1["rates"][to] * amount * 100)/100 , to,Math.round(response1["rates"][to] * 100)/100 );

        Object.keys(countries).forEach(key => {
            let content = "1 " + from + " = " + Math.round(response1["rates"][key] * 100000)/100000 + " " + key;
            if(side){
                leftResults += "<p>" +  content + "</p>\n";
                side = !side;
            }else{
                rightResults += "<p>" +  content + "</p>\n";
                side = !side;
            }

        });
        dLeftResults.innerHTML = leftResults;
        
        dRightResults.innerHTML = rightResults;

    });
}

function removeErrorMsg(){
    let msg = document.getElementById("dErrorMsg");
    msg.innerHTML = "";
}



